package Validation;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Created by Chira Paul on 10/13/2016.
 */
public class ExceptionCommands extends Exception {
    /*
    pre:scanner read,daca folosim 2 scannere de citire nu functioneaza
    post:aruncam erroare daca nu se citeste int
     */
    public static int citesteInteger(Scanner read) throws GeneralException{
        int n = -1;
        try{
            n = read.nextInt();
            read.nextLine();
        }catch(InputMismatchException e){
            read.nextLine();
            throw new GeneralException("Introduce numar intreg!!");
        }

        return n;
    }
}

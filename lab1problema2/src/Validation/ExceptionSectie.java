package Validation;

import Domain.Sectie;

/**
 * Created by Chira Paul on 10/7/2016.
 */
public class ExceptionSectie extends Exception implements Validator<Sectie>{
    /*
    validare sectie
    formam un string cu errori
    post:aruncam erroare cu stringul cu errori
     */
    public void validate(Sectie c) throws GeneralException {
        String err = "Eroare:";
        if (c.getId() < 0)
            err += "id invalid";
        if (c.getNume().length() == 0)
            err += "nume vid";
        if (c.getNrLoc() < 0)
            err += "nrloc invalid";
        if (err != "Eroare:")
            throw new GeneralException(err);
    }
}

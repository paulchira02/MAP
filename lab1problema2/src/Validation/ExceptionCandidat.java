package Validation;

import Domain.Candidat;

/**
 * Created by Chira Paul on 10/7/2016.
 */
public class ExceptionCandidat extends Exception implements Validator<Candidat>{
    /*
    validare candidat
    formam un string cu errori
    post:aruncam erroare cu stringul cu errori
     */
    public void validate(Candidat c) throws GeneralException {
        String err = "Eroare:";
        if (c.getId() < 0)
            err += "id invalid";
        if (c.getNume() == "")
            err += "nume vid";
        if (c.getAdresa() == "")
            err += "adresa vida";
        if (c.getTel() == "")
            err += "telefon vid";
        if (c.getTel().length() != 10)
            err += "telefon invalid(10 numere)";
        if (err != "Eroare:")
            throw new GeneralException(err);
    }
}

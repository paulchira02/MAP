package Validation;

/**
 * Created by Chira Paul on 10/15/2016.
 */
public interface Validator<E> {
    void validate(E enitity) throws GeneralException;
}

package Controller;

import Domain.Sectie;
import Repository.RepositorySectie;
import Repository.RepositorySectie_Iteratia1;
import Utils.MyArray;
import Validation.ExceptionSectie;
import Validation.GeneralException;

import java.util.List;

/**
 * Created by Chira Paul on 10/7/2016.
 */
public class ControllerSectie {
    /*
    instantiem un repository pentru sectii
     */
    private RepositorySectie repS = new RepositorySectie();
    private ExceptionSectie ex = new ExceptionSectie();

    /*
    adaugare sectie
    post:creem un obiect Sectie cu fielduri-le necesare,apelam functia din repo
     */
    public void addSectie(int id, String nume, int nrLoc) throws GeneralException {
        Sectie s = new Sectie(id, nume, nrLoc);
        try {
            ex.validate(s);
        } catch (GeneralException e) {
            System.out.println(e.getMessage());
        }
        if (cautaSectie(id)) {
            throw new GeneralException("id existent");
        }
        repS.save(s);
    }

    /*
    cauta o sectie in array
    post:returneaza true daca se gaseste,fals in caz contrar
     */
    public boolean cautaSectie(int id) {
        List<Sectie> sec = repS.getAll();
        for (int i = 0; i < sec.size(); i++)
            if (sec.get(i).getId() == id)
                return true;
        return false;
    }

    /*
    sterge sectie de pe o anumita pozitie
    in caz ca nu exista aruncam erroare
    post:sectia a fost stearsa
     */
    public void stergeSectie(int id) throws GeneralException {
        if (cautaSectie(id) == false)
            throw new GeneralException("id inexistent");
        repS.delete(pozitieSectie(id));

    }

    /*
    modifica sectie de pe o anumita dupa id
    aruncam erroare daca id-ul nu exista
    post:sectia a fost modificata
     */
    public void modifSectie(int id, int id1, String nume, int nrLoc) throws GeneralException {
        if (cautaSectie(id) == false)
            throw new GeneralException("id inexistent");
        if (cautaSectie(id1)) {
            throw new GeneralException("id existent");
        }
        Sectie s = new Sectie(id1, nume, nrLoc);
        repS.update(s, pozitieSectie(id));
    }

    /*
    returnam sectia dupa id
    post:daca se gaseste sectia in array->returnam sectie
    -1 in caz contrar
     */
    public int pozitieSectie(int id) {
        List<Sectie> sec = repS.getAll();
        int size = sec.size();
        for (int i = 0; i < size; i++)
            if (sec.get(i).getId() == id)
                return i;
        return -1;
    }

    /*
    post:returneaza array-ul cu candidati
     */
    public List<Sectie> getAllS() {
        return repS.getAll();
    }

    /*
    returneaza size-ul
     */
    public int getSizeS() {
        return repS.getAll().size();
    }
}

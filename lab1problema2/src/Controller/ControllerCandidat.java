package Controller;

import Domain.Candidat;
import Repository.RepositoryCandidat;
import Repository.RepositoryCandidat_Iteratia1;
import Utils.MyArray;
import Validation.ExceptionCandidat;
import Validation.GeneralException;

import java.util.List;

/**
 * Created by Chira Paul on 10/8/2016.
 */
public class ControllerCandidat {
    /*
    instantiem un repostiory pt candidati
     */
    private RepositoryCandidat repC = new RepositoryCandidat();
    private ExceptionCandidat ex = new ExceptionCandidat();
    /*
    adugare candidat,se creaza un obiect si se adauga
    post:candidatul a fost adaugat,in caz contarar aruncam exceptie
     */
    public void addCandidat(int id, String nume, String tel, String adresa) throws GeneralException {
        Candidat c = new Candidat(id, nume, tel, adresa);
        try {
            ex.validate(c);
        } catch (GeneralException e) {
            System.out.println(e.getMessage());
        }
        if (cautaCandidat(id)) {
            throw new GeneralException("id existent");
        }
        repC.save(c);
    }
    /*
    returnam true sau false daca candidatul cu id-ul id exista
     */
    public boolean cautaCandidat(int id) {
        List<Candidat> cand = repC.getAll();
        for (int i = 0; i < cand.size(); i++)
            if (cand.get(i).getId() == id)
                return true;
        return false;
    }
    /*
    sterge candidatul cu id-il id
    in caz contrar aruncam eroare daca candidatul nu exista
     */
    public void stergeCandidat(int id) throws GeneralException {
        if (cautaCandidat(id) == false)
            throw new GeneralException("id inexistent");
        repC.delete(pozitieCandidat(id));

    }
    /*
    modificam candidatul,in caz ca nu exista
    aruncam erroare
     */
    public void modifCandidat(int id, int id1, String nume, String tel, String adresa) throws GeneralException {
        if (cautaCandidat(id) == false)
            throw new GeneralException("id inexistent");
        if (cautaCandidat(id1)) {
            throw new GeneralException("id existent");
        }
        Candidat c = new Candidat(id1, nume, tel, adresa);
        repC.update(c, pozitieCandidat(id));
    }
    /*
    returnam pozitia un candidat
     */
    public int pozitieCandidat(int id) {
        //Candidat[] cand = repC.getAll();
        List<Candidat> cand = repC.getAll();
        int size = cand.size();
        for (int i = 0; i < size; i++)
            if (cand.get(i).getId() == id)
                return i;
        return -1;
    }

    public List<Candidat> getAllC() {
        return repC.getAll();
    }

    public int getSizeC() {
        return repC.getAll().size();
    }
}

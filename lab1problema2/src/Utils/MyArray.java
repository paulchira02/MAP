package Utils;


/**
 * Created by Chira Paul on 10/13/2016.
 */
public class MyArray<T> {
    private Object[] array;
    private int cap;
    private int size;

    public MyArray() {
        this.array = new Object[100];
        cap = 100;
        size = 0;
    }

    public MyArray(int cap) {
        this.cap = cap;
        this.array = new Object[cap];
        size = 0;
    }

    public T getElement(int poz) {
        if (poz < size && poz >= 0)
            return (T) array[poz];
        return null;
    }

    public void addElement(T t) {
        if (cap == size) {
            redimensionare();
        }
        array[size] = t;
        size++;
    }

    private void redimensionare() {
        cap = cap * 2;
        Object[] array_sec = new Object[cap];
        for (int i = 0; i < size; i++)
            array_sec[size] = array[size];
        array = array_sec;
    }

    public void deleteElement(int poz) {
        if (poz < size && poz >= 0) {
            for (int i = poz; i < size - 1; i++)
                array[i] = array[i + 1];
            size--;
        }
    }

    public int getSize() {
        return size;
    }
}

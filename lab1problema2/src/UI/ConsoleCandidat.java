package UI;

import Controller.ControllerCandidat;
import Domain.Candidat;
import Utils.MyArray;
import Validation.ExceptionCommands;
import Validation.GeneralException;

import java.util.List;
import java.util.Scanner;

/**
 * Created by Chira Paul on 10/8/2016.
 */
public class ConsoleCandidat {
    private ControllerCandidat ctr = new ControllerCandidat();
    Scanner read = new Scanner(System.in);

    public void addCandidat() {
        System.out.println("id:");
        int id = -1;
        try{
            id = ExceptionCommands.citesteInteger(read);
        }catch (GeneralException e){
            System.out.println(e.getMessage());
            return;
        }
        //read.nextLine();
        System.out.println("nume:");
        String nume = read.nextLine();
        System.out.println("numar telefon:");
        String tel = read.nextLine();
        System.out.println("adresa:");
        String adresa = read.nextLine();
        try {
            ctr.addCandidat(id, nume, tel, adresa);
        } catch (GeneralException e) {
            System.out.println(e.getMessage());
        }
    }

    public void delCandidat() {
        System.out.println("Introduce id:");
        int id = -1;
        try{
            id = ExceptionCommands.citesteInteger(read);
        }catch (GeneralException e){
            System.out.println(e.getMessage());
            return;
        }
        //read.nextLine();
        try {
            ctr.stergeCandidat(id);
        } catch (GeneralException e) {
            System.out.println(e.getMessage());
        }
    }

    public void updCandidat() {
        System.out.println("Introduce id:");
        int id = -1;
        try{
            id = ExceptionCommands.citesteInteger(read);
        }catch (GeneralException e){
            System.out.println(e.getMessage());
            return;
        }
        //read.nextLine();

        System.out.println("Noul id:");
        int id1 = -1;
        try{
            id1 = ExceptionCommands.citesteInteger(read);
        }catch (GeneralException e){
            System.out.println(e.getMessage());
            return;
        }
        //read.nextLine();
        System.out.println("Noul nume:");
        String nume = read.nextLine();
        System.out.println("Noul numar de telefon:");
        String tel = read.nextLine();
        System.out.println("Noua adresa:");
        String adresa = read.nextLine();
        try {
            ctr.modifCandidat(id, id1, nume, tel, adresa);
        } catch (GeneralException e) {
            System.out.println(e.getMessage());
        }
    }

    public void showCandidati() {
        List<Candidat> cand = ctr.getAllC();
        for (int i = 0; i < ctr.getSizeC(); i++) {
            System.out.println(cand.get(i).getId() + " " + cand.get(i).getNume() + " " + cand.get(i).getTel() + " " + cand.get(i).getAdresa());
        }
    }

    public void Menu() {
        System.out.println("5.Adauga candidat");
        System.out.println("6.Afiseaza candidati");
        System.out.println("7.Sterge candidat");
        System.out.println("8.Modifica candidat");
        System.out.println("0.Iesire");

    }

    public void codedCandidat() {
        try {
            ctr.addCandidat(1, "paul", "0755427162", "Cluj-Napoca");
            ctr.addCandidat(2, "laura", "0756427862", "Satu Mare");
            ctr.addCandidat(3, "andrei", "0723457162", "Cluj-Napoca");
            ctr.addCandidat(4, "bogdan", "0755412345", "Bistrita");
            ctr.addCandidat(5, "maria", "0667789162", "Zalau");
        } catch (GeneralException e) {
            System.out.println(e.getMessage());
        }
    }
}

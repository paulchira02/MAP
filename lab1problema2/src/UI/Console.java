package UI;


import Controller.ControllerSectie;
import Domain.Sectie;
import Validation.GeneralException;

import java.util.Scanner;

/**
 * Created by Chira Paul on 10/3/2016.
 */
public class Console {
    ConsoleSectie cs = new ConsoleSectie();
    ConsoleCandidat cd = new ConsoleCandidat();
    Scanner read = new Scanner(System.in);
    /*
    meniu aplicatie
     */
    public void ShowUI() {
        cs.Menu();
        cd.Menu();
        cs.codedSectie();
        cd.codedCandidat();
        System.out.println("Comanda:");
        int cmd = read.nextInt();
        read.nextLine();
        while (true) {
            if (cmd == 1) {
                cs.addSectie();
                System.out.println("Comanda:");
                cmd = read.nextInt();
                read.nextLine();
            }
            if (cmd == 2) {
                cs.showSectie();
                System.out.println("Comanda:");
                cmd = read.nextInt();
                read.nextLine();
            }
            if (cmd == 3) {
                cs.delSectie();
                System.out.println("Comanda:");
                cmd = read.nextInt();
                read.nextLine();
            }

            if (cmd == 4) {
                cs.updSectie();
                System.out.println("Comanda:");
                cmd = read.nextInt();
                read.nextLine();
            }
            if (cmd == 5) {
                cd.addCandidat();
                System.out.println("Comanda:");
                cmd = read.nextInt();
                read.nextLine();
            }
            if (cmd == 6) {
                cd.showCandidati();
                System.out.println("Comanda:");
                cmd = read.nextInt();
                read.nextLine();
            }
            if (cmd == 7) {
                cd.delCandidat();
                System.out.println("Comanda:");
                cmd = read.nextInt();
                read.nextLine();
            }

            if (cmd == 8) {
                cd.updCandidat();
                System.out.println("Comanda:");
                cmd = read.nextInt();
                read.nextLine();
            }
            if (cmd == 0) {
                break;
            }
        }
    }
}

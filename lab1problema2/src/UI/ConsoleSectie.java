package UI;

import Controller.ControllerSectie;
import Domain.Sectie;
import Utils.MyArray;
import Validation.ExceptionCommands;
import Validation.GeneralException;

import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

/**
 * Created by Chira Paul on 10/8/2016.
 */
public class ConsoleSectie {
    private ControllerSectie ctr = new ControllerSectie();
    Scanner read = new Scanner(System.in);

    public void addSectie() {
        System.out.println("id:");
        int id = -1;
        try{
            id = ExceptionCommands.citesteInteger(read);
        }catch(GeneralException e){
            System.out.println(e.getMessage());
            return;
        }
        //read.nextLine();
        System.out.println("nume:");
        String nume = read.nextLine();
        System.out.println("numar loc:");
        int nrLoc = -1;
        try{
            nrLoc = ExceptionCommands.citesteInteger(read);
        }catch(GeneralException e){
            System.out.println(e.getMessage());
            return;
        }
        //read.nextLine();
        try {
            ctr.addSectie(id, nume, nrLoc);
        } catch (GeneralException e) {
            System.out.println(e.getMessage());
        }
    }

    public void delSectie() {
        System.out.println("Introduce id:");
        int id = -1;
        try{
            id = ExceptionCommands.citesteInteger(read);
        }catch(GeneralException e){
            System.out.println(e.getMessage());
            return;
        }
        //read.nextLine();
        try {
            ctr.stergeSectie(id);
        } catch (GeneralException e) {
            System.out.println(e.getMessage());
        }
    }

    public void updSectie() {
        System.out.println("Introduce id:");
        int id = -1;
        try{
            id = ExceptionCommands.citesteInteger(read);
        }catch(GeneralException e){
            System.out.println(e.getMessage());
            return;
        }
        //read.nextLine();

        System.out.println("Noul id:");
        int id1 = -1;
        try{
            id1 = ExceptionCommands.citesteInteger(read);
        }catch (GeneralException e){
            System.out.println(e.getMessage());
            return;
        }
        //read.nextLine();
        System.out.println("Noul nume:");
        String nume = read.nextLine();
        System.out.println("Noul numar loc:");
        int nrLoc = -1;
        try{
            nrLoc = ExceptionCommands.citesteInteger(read);
        }catch (GeneralException e){
            System.out.println(e.getMessage());
            return;
        }
        //read.nextLine();
        try {
            ctr.modifSectie(id, id1, nume, nrLoc);
        } catch (GeneralException e) {
            System.out.println(e.getMessage());
        }
    }

    public void showSectie() {
        List<Sectie> sec = ctr.getAllS();
        for (int i = 0; i < ctr.getSizeS(); i++) {
            System.out.println(sec.get(i).getId() + " " + sec.get(i).getNume() + " " + sec.get(i).getNrLoc());
        }
    }

    public void Menu() {
        System.out.println("1.Adauga sectie");
        System.out.println("2.Afiseaza secti");
        System.out.println("3.Sterge sectie");
        System.out.println("4.Modifica sectie");

    }

    public void codedSectie() {
        try {
            ctr.addSectie(1, "paul", 10);
            ctr.addSectie(2, "laura", 11);
            ctr.addSectie(3, "andrei", 12);
            ctr.addSectie(4, "bogdan", 13);
            ctr.addSectie(5, "maria", 14);
        } catch (GeneralException e) {
            System.out.println(e.getMessage());
        }
    }
}

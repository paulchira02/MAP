package Domain;

/**
 * Created by Chira Paul on 10/7/2016.
 */
public class Candidat {
    /*
    field-uri id,nume,tel,adresa
     */
    private int id;
    private String nume;
    private String tel;
    private String adresa;

    /*
    constructor cu 3 parametri
    pre:id-int,nume-String,tel-String,adresa-String
     */
    public Candidat(int id, String nume, String tel, String adresa) {
        this.id = id;
        this.nume = nume;
        this.tel = tel;
        this.adresa = adresa;
    }

    /*
    getter id
    post:returneaza id
     */
    public int getId() {
        return id;
    }

    /*
     setter id
     post:seteaza noul id
     */
    public void setId(int id) {
        this.id = id;
    }

    /*
    getter nume
    post:returneaza nume
     */
    public String getNume() {
        return nume;
    }

    /*
    setter nume
    post:seteaza noul nume
    */
    public void setNume(String nume) {
        this.nume = nume;
    }

    /*
    getter getTel
    post:returneaza getTel
    */
    public String getTel() {
        return tel;
    }

    /*
    setter tel
    post:seteaza noul tel
    */
    public void setTel(String tel) {
        this.tel = tel;
    }

    /*
    getter adresa
    post:returneaza adresa
    */
    public String getAdresa() {
        return adresa;
    }

    /*
    setter adresa
    post:seteaza noua adresa
    */
    public void setAdresa(String adresa) {
        this.adresa = adresa;
    }
}

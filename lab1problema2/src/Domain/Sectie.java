package Domain;

/**
 * Created by Chira Paul on 10/7/2016.
 */
public class Sectie {
    /*
    field-uri id,nume,nrLoc
     */
    private int id;
    private String nume;
    private int nrLoc;

    /*
    constructor cu 3 parametri
    pre:id-int,nume-String,nrLoc-String
     */
    public Sectie(int id, String nume, int nrLoc) {
        this.id = id;
        this.nume = nume;
        this.nrLoc = nrLoc;
    }

    /*
    getter id
    post:returneaza id
     */
    public int getId() {
        return id;
    }

    /*
    setter id
    post:seteaza un nou id
     */
    public void setId(int id) {
        this.id = id;
    }

    /*
    getter nume
    post:returneaza nume
     */
    public String getNume() {
        return nume;
    }

    /*
    setter nume
    post:seteaza un nou nume
     */
    public void setNume(String nume) {
        this.nume = nume;
    }

    /*
    getter nrLoc
    post:returneaza nrLoc
     */
    public int getNrLoc() {
        return nrLoc;
    }

    /*
     setter nrLoc
     post:seteaza un nou nrLoc
      */
    public void setNrLoc(int nrLoc) {
        this.nrLoc = nrLoc;
    }
}

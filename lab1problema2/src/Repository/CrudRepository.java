package Repository;

/**
 * Created by Chira Paul on 10/14/2016.
 */
public interface CrudRepository<E> {
    void save(E entity);
    void delete(int id);
    void update(E e,int id);
    E findOne(int id);
}

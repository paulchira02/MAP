package Repository;

import Domain.Candidat;
import Utils.MyArray;

/**
 * Created by Chira Paul on 10/8/2016.
 */
public class RepositoryCandidat_Iteratia1 {
    /*
    array ce retine candidati
     */
    private MyArray<Candidat> cand = new MyArray<>();

    public RepositoryCandidat_Iteratia1() {
    }

    /*
    adaugare candidat c
    post:c a fost adaugat in array
    */
    public void adaugaCandidat(Candidat c) {
        cand.addElement(c);
    }

    /*
    stergere candidat c
    post:c a fost sters din array
    */
    public void stergeCandidat(int poz) {
        /*for(int i = poz;i < elems - 1;i++){
            cand[i] = cand[i+1];
        }
        elems--;*/
        cand.deleteElement(poz);
    }

    /*
    modifica candidatuk de pe pozitia poz
    post:c a fost modificat in array
     */
    public void modificaCandidat(Candidat s, int poz) {
        cand.getElement(poz).setNume(s.getNume());
        cand.getElement(poz).setAdresa(s.getAdresa());
        cand.getElement(poz).setId(s.getId());
        cand.getElement(poz).setTel(s.getTel());
    }

    /*
    returnam array-ul
    post:cond a fost returnat
    */
    public MyArray<Candidat> getAll() {
        return cand;
    }

    /*
    returnam size-ul
    */
    public int getSize() {
        return cand.getSize();
    }
}

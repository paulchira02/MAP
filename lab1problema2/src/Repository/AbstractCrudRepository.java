package Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Chira Paul on 10/14/2016.
 */
public abstract class AbstractCrudRepository<E> implements CrudRepository<E>{
    private List<E> store = new ArrayList<E>();
    public void save(E e){
        store.add(e);
    }
    public void delete(int id){
        store.remove(id);
    }
    public void update(E e,int id){
        store.set(id,e);
    }
    public E findOne(int id){
        return store.get(id);
    }
    public List<E> getAll(){
        return store;
    }
}

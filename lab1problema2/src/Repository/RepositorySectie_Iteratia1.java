package Repository;

import Domain.Sectie;
import Utils.MyArray;

/**
 * Created by Chira Paul on 10/7/2016.
 */
public class RepositorySectie_Iteratia1 {
    /*
    array list implementat cu template
    retine un array de sectii
     */
    private MyArray<Sectie> sec = new MyArray<>();

    public RepositorySectie_Iteratia1() {
    }

    /*
    adaugare Sectie s
    post:s a fost adaugat in array
     */
    public void adaugaSectie(Sectie s) {
        sec.addElement(s);
    }

    /*
    stergere Sectie s de pe o pozitie
    post:s a fost sters din array
     */
    public void stergeSectie(int poz) {
        sec.deleteElement(poz);
    }

    /*
    modifica sectia de pe pozitia poz
    post:s a fost modificat in array
     */
    public void modificaSectie(Sectie s, int poz) {
        sec.getElement(poz).setId(s.getId());
        sec.getElement(poz).setNume(s.getNume());
        sec.getElement(poz).setNrLoc(s.getNrLoc());
    }

    /*
    returnam array-ul
    post:sec a fost returnat
    */
    public MyArray<Sectie> getAll() {
        return sec;
    }

    /*
    returnam size-ul
     */
    public int getSize() {
        return sec.getSize();
    }
}
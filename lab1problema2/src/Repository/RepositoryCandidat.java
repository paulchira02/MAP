package Repository;

import Domain.Candidat;

import java.util.List;

/**
 * Created by Chira Paul on 10/15/2016.
 */
public class RepositoryCandidat extends AbstractCrudRepository<Candidat> {
    @Override
    public void save(Candidat candidat) {
        super.save(candidat);
    }

    @Override
    public void delete(int id) {
        super.delete(id);
    }

    @Override
    public void update(Candidat candidat, int id) {
        super.update(candidat, id);
    }

    @Override
    public Candidat findOne(int id) {
        return super.findOne(id);
    }

    @Override
    public List<Candidat> getAll() {
        return super.getAll();
    }
}

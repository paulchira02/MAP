package Repository;

import Domain.Sectie;

/**
 * Created by Chira Paul on 10/14/2016.
 */
public class RepositorySectie extends AbstractCrudRepository<Sectie>{
    @Override
    public void save(Sectie sectie) {
        super.save(sectie);
    }

    @Override
    public void delete(int id) {
        super.delete(id);
    }

    @Override
    public void update(Sectie sectie, int id) {
        super.update(sectie, id);
    }

    @Override
    public Sectie findOne(int id) {
        return super.findOne(id);
    }
}
